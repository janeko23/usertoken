# userToken
# Tabla de contenido

1. [Usuarios](## Usuarios)
2. [Registro](## Registro)
3. [Login](## Login)

## Usuarios

- [ ] <del>/user/register [POST]</del>
- [ ] /user/status [POST]
- [ ] <del>/user/login [POST]</del>
- [ ] <del>/user/authenticate [GET]</del>
- [ ] <del>/user/logout [GET]</del>
- [ ] /user/credit [POST]
- [ ] /user/credito [GET]

----
## Registro
_Users_ |
-----|


Path | Verbo | Parámetros | Comentarios
-----|-------|------------|:------------:
/user/register|POST| <ul><li>username:_String_ </li><li>password:_String_</li><li>email:_String_</li></ul>| Agrega un usuario|
/user/status|POST|<ul><il>status:_boolean_</li></ul>|Establece la habilitación o inhabilitación del usuario(el username se obtiene del token)
/user/login|POST|<ul><li>email:_String_ </li><li>password:_String_</li></ul>|Devuelve un token único para ese usuario

Registrar usuario|
--|

  **POST**|_url_ | http://localhost:3000/users/register
--|--|--

Body|
--|
```json
{
    "username": "Wonky",
    "password": "123456",
    "email": "user1@example.com",
}
```


Request|
--|
```json
{
    "auth": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYzY2NTZhMGVkZWIzMDAxZmM0MWRmZCIsImlhdCI6MTYwNjgzNzYxMSwiZXhwIjoxNjA2OTI0MDExfQ.HZuxMvDngBITN81IdaFbolfQ6ab69KBZkUZepUtn2q0"
}
```
![Docker](img/register.png) 
----
## Login

LOGIN|
--|

POST | _url_ | http://localhost:3000/users/login
--|--|--

Body|
--|
```json
{
    "email":"user1@example.com",
    "password":"123456"
}
```

Request|
--|

```json
  {
    "auth": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYzY2Mzg4MGVkZWIzMDAxZmM0MWRmYSIsImlhdCI6MTYwNjgzNzY5OCwiZXhwIjoxNjA2OTI0MDk4fQ.QU9kLalbyUvezovNcj4kN0Gsj2BhBpjIoEB6iDAiTls"
}
```
![Docker](img/login.png)
---
#  Authenticate
Authenticate|
--|

GET | _url_ | http://localhost:3000/users/authenticate
--|--|--
```

HEADERS|
--|

```x-access-token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYzY2Mzg4MGVkZWIzMDAxZmM0MWRmYSIsImlhdCI6MTYwNjgzNzY5OCwiZXhwIjoxNjA2OTI0MDk4fQ.QU9kLalbyUvezovNcj4kN0Gsj2BhBpjIoEB6iDAiTls
```
Request|
--|

```json
  {
    "_id": "5fc663880edeb3001fc41dfa",
    "username": "Wonky",
    "email": "user1@example.com",
    "status": true,
    "__v": 0
}
```
![Docker](img/auth.png)
---