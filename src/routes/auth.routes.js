const { Router } = require("express");
const router = Router();
const verifyToken = require('../middlewares/verifyToken')
const checkDuplicateUsernameOrEmail = require('../middlewares/verifySignup')
const authCtrl = require("../controllers/auth.controller");

router.post("/register", checkDuplicateUsernameOrEmail, authCtrl.signUp);
router.post("/login", authCtrl.signIn);
router.get("/authenticate", verifyToken, authCtrl.verifyToken);
router.get("/logout", authCtrl.logout);
module.exports = router;