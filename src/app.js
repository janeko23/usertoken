const express = require('express');
const morgan = require('morgan');
const app = express();
const bodyParser = require('body-parser');
const pkg = require("../package.json");
const mongoose = require('./config/database'); //database configuration
mongoose.connection.on('error', console.error.bind(console, 'Error de conexion en MongoDB'));
// settings
app.set('port', process.env.PORT || 4000);

// middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}))
//app.use(express.urlencoded({ extended: false}));
app.use(express.json());


// Routes
app.get("/", (req, res) => {
  res.json({
    message: "Welcome to Delivery API",
    name: pkg.name,
    version: pkg.version,
    description: pkg.description,
    author: pkg.author,
  });
});

app.use('/users',require('./routes/auth.routes'));

module.exports = app;
