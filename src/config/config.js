const config  = require("dotenv");

module.exports = {
  MONGODB_URI: process.env.MONGODB_URI || "",
  PORT: process.env.PORT || 4000,
  SECRET: 'mysecret'
};